AWS OpsWorks MongoDB Replicaset
===============================

Chef Recipe for a MongoDB Replicaset running in AWS OpsWorks

Detailed instructions http://netinlet.com/blog/2014/01/18/setting-up-a-mongodb-replicaset-with-aws-opsworks/

/opt/aws/opsworks/local/bin/ruby /opt/aws/opsworks/current/bin/chef-client -j /var/lib/aws/opsworks/chef/2014-06-13-01-23-27-01.json -c /var/lib/aws/opsworks/client.rb -o opsworks_custom_cookbooks::execute
